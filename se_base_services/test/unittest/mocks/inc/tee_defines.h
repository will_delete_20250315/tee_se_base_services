/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_DEFINES_H
#define UNIT_TEST_INC_TEE_DEFINES_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include <stddef.h>
#include <stdint.h>

typedef uint32_t TEE_Result;

enum TEE_Result_Value {
    TEE_SUCCESS = 0x0,                     /* success */
    TEE_ERROR_READ_DATA = 6,               /* read data failed */
    TEE_ERROR_BAD_PARAMETERS = 0xFFFF0006, /* incorrect parameters */
    TEE_ERROR_ITEM_NOT_FOUND = 0xFFFF0008, /* cannot find target item */
    TEE_ERROR_MAC_INVALID = 0xFFFF3071,    /* MAC operation failed */

    TEE_FAIL = 0xFFFF5002,                 /* system error */
};

#define NODE_LEN 8
typedef struct tee_uuid {
    uint32_t timeLow;
    uint16_t timeMid;
    uint16_t timeHiAndVersion;
    uint8_t clockSeqAndNode[NODE_LEN];
} TEE_UUID;

typedef struct {
    uint32_t objectType;
    uint32_t objectSize;
    uint32_t maxObjectSize;
    uint32_t objectUsage;
    uint32_t dataSize;
    uint32_t dataPosition;
    uint32_t handleFlags;
} TEE_ObjectInfo;

typedef struct {
    uint32_t attributeID;
} TEE_Attribute;

enum TEE_ObjectAttribute {
    TEE_ATTR_SECRET_VALUE = 0xC0000000,
};

enum TEE_ObjectType {
    TEE_TYPE_AES = 0xA0000010,
};

#define OBJECT_NAME_LEN_MAX 255

struct TEE_ObjectHandleInner {
    void *dataPtr;
    uint32_t dataLen;
    uint8_t dataName[OBJECT_NAME_LEN_MAX];
    TEE_ObjectInfo *objectInfo;
    TEE_Attribute *attribute;
    uint32_t attributesLen;
    uint32_t crtMode;
    void *infoattrfd;
    uint32_t generateFlag;
    uint32_t storageId;
};

typedef struct TEE_ObjectHandleInner *TEE_ObjectHandle;
typedef struct TEE_ObjectEnumHandleInner *TEE_ObjectEnumHandle;
typedef struct TEE_OperationHandleInner *TEE_OperationHandle;

typedef struct {
    uint32_t seconds;
    uint32_t millis;
} TEE_Time;

#endif // UNIT_TEST_INC_TEE_DEFINES_H