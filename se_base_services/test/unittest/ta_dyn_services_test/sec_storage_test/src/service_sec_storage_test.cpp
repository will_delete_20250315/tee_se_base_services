/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include <securec.h>

#include "service_sec_storage.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace testing;

TEST(ServiceSecStoragetTest, SecStorageProcessCommand)
{
    SharedDataBuffer buffer = {.data = nullptr, .dataSize = 0, .dataMaxSize = 0};
    auto ret = SecStorageProcessCommand(0, 0, &buffer);
    EXPECT_EQ(ret, INVALID_PARA_ERR_CMD);
}

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS