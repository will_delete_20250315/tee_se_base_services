/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DYN_SERVICE_INC_SE_CARD_CHANNEL_SCP_H
#define DYN_SERVICE_INC_SE_CARD_CHANNEL_SCP_H

#include <tee_internal_se_api.h>
#include <tee_object_api.h>

#include "se_base_services_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

TEE_ObjectHandle CreateKeyObject(const uint8_t *key, uint32_t size);

void DeleteKeyObject(TEE_ObjectHandle object);

bool CreateScpKeyObjects(const uint8_t *key, uint32_t keyLen, TEE_ObjectHandle *enc, TEE_ObjectHandle *mac);

TEE_SC_Params *CreateScpParams(const uint8_t *key, uint32_t keyLen, uint8_t keyVersion, uint8_t keyId);

void DeleteScpParams(TEE_SC_Params *params);

#ifdef __cplusplus
}
#endif

#endif // DYN_SERVICE_INC_SE_CARD_CHANNEL_SCP_H