/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DYN_CLIENT_INC_STORAGE_H
#define DYN_CLIENT_INC_STORAGE_H

#include <stdint.h>

#include "se_module_sec_storage_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode SeSecStorageSetFactoryResetAuthenticationKey(FactoryResetLevel level, FactoryResetAuthAlgo algo,
    const StorageAuthKey *key);

ResultCode SeSecStorageGetFactoryResetAuthenticationAlgo(FactoryResetLevel level, FactoryResetAuthAlgo *algo);

ResultCode SeSecStoragePrepareFactoryReset(uint8_t *nonce, uint32_t *length);

ResultCode SeSecStorageProcessFactoryReset(FactoryResetLevel level, const uint8_t *credential, uint32_t length);

ResultCode SeSecStorageSetToUserMode(const StorageUserModeConf *config);

ResultCode SeSecStorageIsSlotOperateAlgorithmSupported(SlotOperAlgo algo, uint32_t *avaliable);

ResultCode SeSecStorageIsFactoryResetAlgorithmSupported(FactoryResetAuthAlgo algo, uint32_t *avaliable);

ResultCode SeSecStorageAllocateSlot(const char *name, const StorageSlotAttr *slotAttr, const StorageAuthKey *slotKey);

ResultCode SeSecStorageGetSlotStatus(const char *name, StorageSlotStatus *status);

ResultCode SeSecStorageWriteSlot(const char *name, const StorageAuthKey *key, uint16_t offset, uint16_t length,
    const StorageDataBuffer *data);

ResultCode SeSecStorageReadSlot(const char *name, const StorageAuthKey *key, uint16_t offset, uint16_t length,
    StorageDataBuffer *data);

ResultCode SeSecStorageFreeSlot(const char *name, const StorageAuthKey *key);

ResultCode SeSecStorageSetAllSlotsSize(const uint16_t *slotSizeArray, uint32_t arrayLength);

#ifdef __cplusplus
}
#endif

#endif // DYN_CLIENT_INC_STORAGE_H