/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_FUNCTION_MOCKER_H
#define UNIT_TEST_INC_FUNCTION_MOCKER_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {

template <typename T>
class FunctionMocker {
public:
    FunctionMocker()
    {
        instance_ = static_cast<T *>(this);
    }

    virtual ~FunctionMocker()
    {
        instance_ = nullptr;
    }

    static inline T *GetInstance()
    {
        return instance_;
    }

private:
    static T *instance_;
};

template <typename T>
T *FunctionMocker<T>::instance_ = nullptr;

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS
#endif // UNIT_TEST_INC_FUNCTION_MOCKER_H
