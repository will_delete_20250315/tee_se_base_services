# interface
add_library(function_mocker INTERFACE)
target_include_directories(function_mocker INTERFACE inc)

# mocks
add_library(
    se_base_test_mocks_obj OBJECT
    src/logger_mock.c
    src/channel_operations_mock.cpp
    src/ipc_transmit_mock.cpp
)

target_include_directories(se_base_test_mocks_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(se_base_test_mocks_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})

target_link_libraries(se_base_test_mocks_obj PUBLIC function_mocker)
target_link_libraries(se_base_test_mocks_obj PRIVATE se_apdu_core_obj)
target_link_libraries(se_base_test_mocks_obj PRIVATE logger_obj)
target_link_libraries(se_base_test_mocks_obj PRIVATE se_base_services_defines)
target_link_libraries(se_base_test_mocks_obj PRIVATE GTest::gmock)
