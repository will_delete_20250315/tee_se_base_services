add_library(se_base_services_defines INTERFACE)
target_include_directories(se_base_services_defines INTERFACE .)

file(
    GLOB
    se_base_fwk_intf_headers
    *.h
)
file(COPY ${se_base_fwk_intf_headers} DESTINATION "${CMAKE_SOURCE_DIR}/output/")
