/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTERFACES_SE_BASE_SERVICES_DEFINES
#define INTERFACES_SE_BASE_SERVICES_DEFINES

#include <stdint.h>

typedef enum {
    SUCCESS = 0,
    INVALID_PARA_NULL_PTR,
    INVALID_PARA_ERR_CMD,
    INVALID_PARA_ERR_SIZE,
    INVALID_PARA_ERR_VALUE,
    INVALID_PERM,
    MEM_ALLOC_ERR,
    MEM_READ_ERR,
    MEM_COPY_ERR,
    MEM_WRITE_ERR,
    IPC_TRANSMIT_ERR,
    IPC_PARA_PROCESS_ERR_INVALID_VALUE,
    IPC_PARA_PROCESS_ERR_INDEX_1,
    IPC_PARA_PROCESS_ERR_INDEX_2,
    IPC_PARA_PROCESS_ERR_INDEX_3,
    IPC_PARA_PROCESS_ERR_INDEX_4,
    IPC_PARA_PROCESS_ERR_INDEX_5,
    IPC_PARA_PROCESS_ERR_INDEX_6,
    IPC_RES_FILL_UP_ERR,
    IPC_RES_DATA_ERR,
    CHN_OPEN_ERR,
    CHN_SCP_OPEN_ERR,
    CHN_CLOSE_ERR,
    CMD_APDU_CREATE_ERR,
    CMD_APDU_TRANS_ERR,
    RES_APDU_LENGTH_ERR,
    RES_APDU_FMT_ERR,
    RES_APDU_SW_ERR,
    RES_APDU_DATA_ERR,
    RES_APDU_DATA_ERR_INDEX_1,
    RES_APDU_DATA_ERR_INDEX_2,
    RES_APDU_DATA_ERR_INDEX_3,
    RES_APDU_DATA_ERR_INDEX_4,
    RES_APDU_DATA_ERR_INDEX_5,
    RES_APDU_DATA_ERR_INDEX_6,
    NOT_SUPPORT,
    ERR_GENERIC_ERR = 0x400,
    ERR_FATAL = 0xAAAA,
} ResultCode;

enum {
    CMD_IS_SERVICE_AVAILABLE = 100,
    CMD_IS_SERVICE_AVALIABLE = 100,
    CMD_SET_SERVICE_CONFIG = 101,
    CMD_SET_BINDING_KEY = 201,
    CMD_DELETE_INIT_KEY = 202,
};

#define SERVICE_WITH_CMD_ID(service, cmd) (((service) << 16) | (cmd))

typedef enum ServiceId {
    SERVICE_ID_PIN_AUTH = 0x100,
    SERVICE_ID_SEC_STORAGE = 0x200,
} ServiceId;

typedef struct SharedDataBuffer {
    uint8_t *data;
    uint32_t dataSize;
    uint32_t dataMaxSize;
} SharedDataBuffer;

typedef ResultCode IpcTransmit(uint32_t cmd, const uint8_t *data, uint32_t dataLen, uint8_t *reply, uint32_t *replyLen);

#endif // INTERFACES_SE_BASE_SERVICES_DEFINES