# 通用规范

- OpenHarmony安全芯片管理框架高安业务通用规范
    - [前言](1.md)
    - [概述](2.md)
    - [OpenHarmony安全芯片管理框架](3.md)
    - [OpenHarmony安全芯片高安业务](4.md)
    - [OpenHarmony安全芯片约束](5.md)
